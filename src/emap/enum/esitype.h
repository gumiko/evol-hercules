// Copyright (c) Copyright (c) Hercules Dev Team, licensed under GNU GPL.
// Copyright (c) 2014 - 2015 Evol developers

#ifndef EVOL_MAP_ENUM_ESITYPE
#define EVOL_MAP_ENUM_ESITYPE

enum esi_type
{
    SI_PHYSICAL_SHIELD = 966,
    //SI_EVOL_INCSTR = 970,
    SI_EVOL_INCAGI = 971,
    SI_EVOL_INCVIT = 972,
    SI_EVOL_INCINT = 973,
    SI_EVOL_INCDEX = 974,
    SI_EVOL_INCLUK = 975,
    SI_EVOL_INCHIT = 976,
    SI_EVOL_INCFLEE = 977,
    SI_EVOL_WALKSPEED = 978,
    SI_EVOL_INCMHPRATE = 979,
    SI_EVOL_INCMSPRATE = 980,
};

#endif  // EVOL_MAP_ENUM_ESITYPE
